import XCTest
@testable import iCodeTests

XCTMain([
    testCase(iCodeTests.allTests),
])
