//
//  Date+iCode.swift
//  iCode
//  Created by Mohamed Shafraz on 20/10/2014.
/*
 
 Copyright (c) 2014, Shafraz Buhary
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 
 Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 Neither the name of the Copyright holder  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 */

import Foundation

enum DateError: Error {
    case InvalidDateString
}

public extension Date {
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    
    var hour: Int {
        return Calendar.current.component(.hour, from: self)
    }
    
    var minute: Int {
        return Calendar.current.component(.minute, from: self)
    }
    
    var second: Int {
        return Calendar.current.component(.second, from: self)
    }
    
    var dateOnly: Date {
        return Date(year: self.year, month: self.month, day: self.day)
    }
    
    var timeOnly: Date {
        return Date(hour: self.hour, minute: self.minute, second: self.second)
    }
    
    /*Creates Date from given string in given formate */
    
    init(dateString:String, formate:String) throws {
        let dateFormetter = DateFormatter()
        dateFormetter.dateFormat = formate
        guard let date = dateFormetter.date(from: dateString) else {
            throw DateError.InvalidDateString
        }
        self.init(timeInterval:0, since:date)
    }
    
    /*Creates Date with given year, month and day*/
    
    init(year:Int, month:Int, day:Int, timeZoneAbbreviation: String? = nil) {
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        
        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        if let timeZone = timeZoneAbbreviation {
            calendar.timeZone = TimeZone(abbreviation: timeZone)!
        }
        
        let date = calendar.date(from: dateComponents)
        self.init(timeInterval:0, since:date!)
    }
    
    /*Current date with hour, minute and second*/
    
    init(hour:Int, minute:Int, second:Int) {
        var date = Foundation.Date()
        date = date.dateWith(hour: hour, minute: minute, second: second)
        self.init(timeInterval:0, since:date)
    }
    
    /*Returns date string in given formate*/
    func dateTime(inFormate formate:String) ->String {
        let dateFormetter = DateFormatter()
        dateFormetter.dateFormat = formate
        return dateFormetter.string(from: self)
    }
    
    func dateTime(inFormate formate:String, timeZone:TimeZone) ->String {
        let dateFormetter = DateFormatter()
        dateFormetter.dateFormat = formate
        dateFormetter.timeZone = timeZone
        return dateFormetter.string(from: self)
    }
    
    func dateTime(inFormate formate:String, timeZoneName:String) ->String? {
        
        if let timeZone = TimeZone(identifier: timeZoneName) {
            return self.dateTime(inFormate: formate, timeZone: timeZone)
        }
        return nil
    }
    
    static func currentDateTime(inFormate formate:String) ->String {
        let currentDateTime = Foundation.Date()
        return currentDateTime.dateTime(inFormate:formate)
    }
    
    func utcDateTime(inFormate formate:String) ->String? {
        return self.dateTime(inFormate:formate, timeZoneName: "UTC")
    }
    
    static func currentUtcDateTimeInFormate(_ formate:String) ->String? {
        let currentDateTime = Foundation.Date()
        return currentDateTime.utcDateTime(inFormate:formate)
    }
    
    func dateByAdding(seconds:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(seconds))
    }
    
    func dateByAdding(minutes:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(60 * minutes))
    }
    
    func dateByAdding(hours:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(3600 * hours))
    }
    
    func dateByAdding(days:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(86400 * days))
    }
    
    func dateByAdding(weeks:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(604800 * weeks))
    }
    
    func timeDiffInMinutesSinceDate(_ date:Foundation.Date, ignoreSeconds:Bool) ->Int {
        var selfTimeInterval = self.timeIntervalSince1970
        var targatTimeInterval = date.timeIntervalSince1970
        
        /* Removing seconds componants */
        if ignoreSeconds {
            selfTimeInterval = selfTimeInterval / 60;
            targatTimeInterval = targatTimeInterval / 60;
        }
        
        return Int(selfTimeInterval - targatTimeInterval)
    }
    
    
    func dateWith(hour:Int, minute:Int, second:Int) ->Date {
        
        let calendar = Calendar.current
        
        var components = calendar.dateComponents([.hour,.minute,.second], from: self)
        
        components.hour = hour
        components.minute = minute
        components.second = second
        
        return calendar.date(from: components)!
    }
    

    /*Returns date string in given formate*/
    func dateTimeInFormate(_ formate:String) ->String {
        let dateFormetter = DateFormatter()
        dateFormetter.dateFormat = formate
        return dateFormetter.string(from: self)
    }
    
    func dateTimeInFormate(_ formate:String, timeZone:TimeZone) ->String {
        let dateFormetter = DateFormatter()
        dateFormetter.dateFormat = formate
        dateFormetter.timeZone = timeZone
        return dateFormetter.string(from: self)
    }
    
    func dateTimeInFormate(_ formate:String, timeZoneName:String) ->String? {
        
        if let timeZone = TimeZone(identifier: timeZoneName) {
            return self.dateTimeInFormate(formate, timeZone: timeZone)
        }
        return nil
    }
    
    static func currentDateTimeInFormate(_ formate:String) ->String {
        let currentDateTime = Foundation.Date()
        return currentDateTime.dateTimeInFormate(formate)
    }
    
    func utcDateTimeInFormate(_ formate:String) ->String? {
        return self.dateTimeInFormate(formate, timeZoneName: "UTC")
    }
    
    
    func dateByAddingSeconds(_ seconds:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(seconds))
    }
    
    func dateByAddingMinutes(_ minutes:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(60 * minutes))
    }
    
    func dateByAddingHours(_ hours:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(3600 * hours))
    }
    
    func dateByAddingDays(_ days:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(86400 * days))
    }
    
    func dateByAddingWeeks(_ weeks:Int) ->Foundation.Date {
        return self.addingTimeInterval(TimeInterval(604800 * weeks))
    }
    
    
    func dateWithHour(_ hour:Int, minute:Int, second:Int) ->Date {
        
        let calendar = Calendar.current
        
        var components = calendar.dateComponents([.hour,.minute,.second], from: self)
        
        components.year = 0
        components.month = 0
        components.day = 0
        components.hour = hour
        components.minute = minute
        components.second = second
        
        return calendar.date(from: components)!
    }
}


