//
//  Decodable.swift
//  iKanTrack
//
//  Created by Mohamed Shafraz on 1/1/18.
//  Copyright © 2018 KanCoders. All rights reserved.
//

import Foundation

public extension Decodable {
    
    static func decode(data: Data) throws -> Self {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601)
        return try decoder.decode(Self.self, from: data)
    }
    
    static func decode(json: String) throws -> Self {
        return try Self.decode(data: json.data)
    }
    
    init(from info:[String:Any]) throws {
        let data = try JSONSerialization.data(withJSONObject: info, options: [])
        self = try Self.decode(data: data)
    }
    
    init(from data:Data ) throws {
        self = try Self.decode(data: data)
    }
    
    init(from string: String) throws {
        self = try Self.decode(json: string)
    }
}


public extension Encodable {
    
    func encode(dateFormate: String? = nil) throws -> Data {
        let formatter = dateFormate == nil ? DateFormatter.iso8601 : DateFormatter.getFormatter(format: dateFormate!)
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(formatter)
        encoder.outputFormatting = .prettyPrinted
        return try encoder.encode(self)
    }
    
    func json() throws-> String {
        return try self.encode().toString!
    }
}

