//
//  Dictionary.swift
//  iCode
//
//  Created by Shafraz Buhary on 16/7/18.
//

import Foundation

extension Dictionary {
    public var data: Data? {
        do {
            return try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        } catch {
            return nil
        }
    }
    
    public var jsonString: String? {
        return data?.toString
    }
}
