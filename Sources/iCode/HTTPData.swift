//
//  HTTPData.swift
//  iKanTrack
//
//  Created by Mohamed Shafraz on 1/2/18.
//  Copyright © 2018 KanCoders. All rights reserved.
//

import Foundation

public protocol HTTPSubmitable {
    var contantType:String {get}
}

public class HTTPData: HTTPSubmitable {
    public var contantType: String{
        return ""
    }
    
    let boundary:String
    public var data:Data
    
    public init(boundary:String) {
        self.data = Data()
        self.boundary = boundary
    }
    
    public convenience init() {
        self.init(boundary: "------------------4737809831466499882746641449V2ymHFg03ehbqgZCaKO6jy")
    }
}

//extension HTTPData {
//
//    init() {
//        self.init(boundary: "------------------4737809831466499882746641449V2ymHFg03ehbqgZCaKO6jy")
//    }
//
//    init(boundary:String) {
//        self.init()
//        self.boundary = boundary
//    }
//}


