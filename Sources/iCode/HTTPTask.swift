//
//  HTTPTask.swift
//  iCode
//
//  Created by Mohamed Shafraz on 12/25/17.
/*
 
 Copyright © 2017, Shafraz Buhary
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 
 Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 Neither the name of the Copyright holder  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 */

import Foundation

public typealias HTTPTaskHandler = (HTTPURLResponse?,Data?,Error?) -> Void

open class HTTPTask {
    
    private var request: URLRequest
    public var data:HTTPData?
    
    private var sessionConfiguration = URLSessionConfiguration.default
    public var handler: HTTPTaskHandler?
    
    public convenience init(request: URLRequest) {
        self.init(request: request) { (_, _, _) in}
    }
    
    public init(request: URLRequest, handler: @escaping HTTPTaskHandler) {
        self.request = request
        self.handler = handler
    }
    
    public init(urlString: String, method: HTTPMethod, handler: @escaping HTTPTaskHandler) throws {
        guard let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = URL(string:encodedUrl) else {
                throw URLError(.badURL)
        }
        
        request = URLRequest(url:url)
        request.httpMethod = method.rawValue
        self.handler = handler
    }
    
    public convenience init(urlString: String, method: HTTPMethod) throws {
        try self.init(urlString: urlString, method: method, handler: { (_, _, _) in})
    }
    
    public func run() {
        
        if let httpData = self.data  {
            request.setValue(httpData.contantType, forHTTPHeaderField: "Content-Type")
            request.httpBody = httpData.data
            request.setValue("\(httpData.data.count)", forHTTPHeaderField: "Content-Length")
        }
        
        
        let urlSession = URLSession(configuration: sessionConfiguration)
        let sessionTask = urlSession.dataTask(with: request) { (data, response, error) in
            self.handler?(response as? HTTPURLResponse,data,error)
        }
        
        sessionTask.resume()
    }
    
    public func setHeaderProperty(key:String, value:String)  {
        self.request.setValue(value, forHTTPHeaderField: key)
    }
    
    public func setAuthorization(value:String) {
        setHeaderProperty(key: "Authorization", value: value)
    }
}

