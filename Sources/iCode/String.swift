//
//  String.swift
//  iKanTrack
//
//  Created by Mohamed Shafraz on 12/25/17.
/*
 
 Copyright © 2017, Shafraz Buhary
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 
 Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 Neither the name of the Copyright holder  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 */

import Foundation

public extension String {
    
//    public var localized: String {
//        return NSLocalizedString(self, comment: "")
//    }
    
    #if os(Linux)
    
    public init(contentsOfFile path: String)  throws{
        try self.init(contentsOfFile: path, encoding: .utf8)
    }
    
    #endif
    
    var trim: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var hexValue: Int? {
        var value:UInt32 = 0
        if Scanner(string: self).scanHexInt32(&value) {
            return Int(value)
        }
        return nil
    }
    
    var intValue: Int? {
        return Int(self)
    }
    
    var floatValue: Float? {
        return Float(self)
    }
    
    var lengh: Int {
        return self.utf8.count
    }
    
    var isValidString: Bool {
        return !self.trim.isEmpty
    }
    
    var urlEncodedString: String? {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    var urlDecodedString: String {
        return self.removingPercentEncoding!.replacingOccurrences(of: "+", with: " ")
    }
    
    var base64EncodedString: String? {
        return self.base64EncodedString()
    }
    
    func base64EncodedString(options: Data.Base64EncodingOptions = []) -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString(options: options)
        }
        return nil
    }
    
    var base64DecodedString: String? {
        if let data = Data(base64Encoded: self) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    var data: Data {
        return self.data(using: .utf8)!
    }
    
//    public func isValidForPattern(_ pattern:String) ->Bool{
//        return isValidForPattern(pattern, option: NSRegularExpression.Options.caseInsensitive)
//    }
    
//    public func isValidForPattern(_ pattern:String, option:NSRegularExpression.Options) ->Bool {
//
//        var isValid = false
//
//
//        let regex: NSRegularExpression?
//        do {
//            regex = try NSRegularExpression(pattern: pattern, options: option)
//        } catch {
//            regex = nil
//        }
//
//        let textRange = NSMakeRange(0, self.lengh)
//        let matchRange = regex?.rangeOfFirstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportProgress, range: textRange)
//
//        if textRange.location == matchRange?.location && textRange.length == matchRange?.length {
//            isValid = true
//        }
//
//        return isValid
//    }
}

