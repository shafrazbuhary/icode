//
//  HTTPMultipartForm.swift
//  iKanTrack
//
//  Created by Mohamed Shafraz on 1/2/18.
//  Copyright © 2018 KanCoders. All rights reserved.
//

import Foundation
//import UIKit

public class HTTPMultipartForm: HTTPData {
    
    public override init(boundary: String) {
        super.init(boundary: boundary)
    }
    
    public convenience init() {
        self.init(boundary: "------------------4737809831466499882746641449V2ymHFg03ehbqgZCaKO6jy" )
    }
    
    public override var contantType: String{
        return "multipart/form-data; boundary=\(boundary)"
    }
    
    public func wrap() {
        data.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    }
    
    private func addBoundary() {
        data.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    }
    
    public func add(_ name:String,value:String) {
        addBoundary()
        data.append("Content-Disposition:form-data; name= \"\(name)\"\r\n\r\n\(value)\r\n".data(using: String.Encoding.utf8)!)
    }
    
    public func add(json value: String, name: String) {
        print(value)
        addBoundary()
        data.append("Content-Disposition: form-data; name=\"\(name)\"\r\n".data(using: String.Encoding.utf8)!)
        data.append("Content-Type: application/json\r\n\r\n".data(using: String.Encoding.utf8)!)
        data.append(value.data(using: String.Encoding.utf8)!)
        data.append("\r\n".data(using: String.Encoding.utf8)!)
    }
    
    public func add(json value: Encodable, name:String) throws {
        add(json: try value.json() , name: name)
    }
    
    public func add(jpegImage image: Data, name: String, fileName: String) {
        addBoundary()
        data.append("Content-Disposition: form-data; name\"\(name)\"; filename=\"\(fileName).jpg\"\r\n".data(using: String.Encoding.utf8)!)
        data.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
        data.append(image)
        data.append("\r\n".data(using: String.Encoding.utf8)!)
    }
    
    public func add(_ name:String,value:Int) {
        add(name, value: "\(value)")
    }
    
    public func add(_ name:String,value:UInt) {
        add(name, value: "\(value)")
    }
    
    public func add(_ name:String,value:UInt64) {
        add(name, value: "\(value)")
    }
    
    public func add(_ name:String,value:UInt32) {
        add(name, value: "\(value)")
    }
    
    public func add(_ name:String, value:Float) {
        add(name, value: "\(value)")
    }
    
    public func add(_ name:String, value:Date) {
        add(name, date: value, formate: "yyyy-MM-dd HH:mm:ss")
    }
    
    public func add(_ name:String, date:Date, formate:String) {
        add(name, value: date.dateTime(inFormate: formate))
    }
    
    public func add(_ name:String, value:Bool) {
        add(name, value: value ? 1 : 0)
    }
    
//    func add(jpegImage image:UIImage, name: String,fileName: String) {
//        add(jpegImage: UIImageJPEGRepresentation(image,0.0)!, name: name, fileName: fileName)
//    }
}

