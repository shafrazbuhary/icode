//
//  HTTPForm.swift
//  iCode
//
//  Created by Mohamed Shafraz on 1/13/18.
//

import Foundation

public class HTTPForm: HTTPData{
    override public var contantType: String {
        return "application/x-www-form-urlencoded; charset=utf-8"
    }
}
