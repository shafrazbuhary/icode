//
//  HTTPJson.swift
//  iCode
//
//  Created by Shafraz Buhary on 24/7/18.
//

import Foundation

public class HTTPJson: HTTPData {
    override public var contantType: String {
        return "application/json"
    }
}
